package main


import "os"
import "fmt"
import "strings"
import "flag"
import "database/sql"
import _ "github.com/lib/pq"
import _ "github.com/mattn/go-sqlite3"
import "github.com/tg/pgpass"
import "io/ioutil"
import "runtime/pprof"
import "reducer"

func main() {
    var writeTo, templateName, profiling, dbUrl string
    bla   := reducer.NewReducer()
    help  := false

    flag.BoolVar  (&bla.Verbose         , "v", false, "verbose")
    flag.StringVar(&bla.InitialSQLString, "I", "", "initial SQL to send")
    flag.StringVar(&bla.CommentSymbol   , "c", "", "Comment character")
    flag.StringVar(&templateName    , "i", "", "Template filename (- for stdin)")
    flag.StringVar(&writeTo             , "o", "", "Output filename (- for stdout)")
    flag.StringVar(&dbUrl           , "u", "postgres://test:test12@localhost/evlog", "Database URL")
    flag.StringVar(&bla.DriverName      , "d", "sqlite3", "Driver name, protocol-part of url takes precedence.")
    flag.StringVar(&profiling      , "p", "", "Filename for profiling infomation")
    flag.BoolVar  (&help                , "h", false, "Help")
    flag.Parse()

    if help {
        fmt.Fprintln( os.Stderr, os.Args[0], " [options] [name:value pairs] [templatefile] [reportfile]")
        flag.PrintDefaults()
        fmt.Fprintln( os.Stderr, "double-up ':' in filenames outside the optionslist.")
        os.Exit(0)
    }

    if profiling != "" {
        f, err := os.Create( profiling )
        if err != nil {
            fmt.Fprintln(os.Stderr, fmt.Sprintf( "Writing profiling output failed (%v)", err ) )
        } else {
            pprof.StartCPUProfile( f )
            defer pprof.StopCPUProfile()
        }
    }
    if dbUrl != "" {
        if res, err := pgpass.UpdateURL( dbUrl ); err != nil {
           fmt.Fprintln(os.Stderr, fmt.Sprintf( "Fetching database password failed; continuing without (%v)", err ) )
        } else {
           dbUrl = res
        }
    }
    for i := 0; flag.Arg( i ) != ""; i += 1 {
        EndOfParameterName := strings.Index( flag.Arg( i ), ":")
        if EndOfParameterName != -1 && flag.Arg( i )[ EndOfParameterName+1 ] != ':'  {
        ParameterName := flag.Arg( i )[:EndOfParameterName]
        EndOfParameterName += 1
        ParameterValue := flag.Arg( i )[EndOfParameterName:]
        if bla.Verbose {
            fmt.Fprintln( os.Stderr, fmt.Sprintf("%v%v%v%v%v", "param: ", 
                                                 " adding parameter value ", ParameterValue, 
                                                 " by name ", ParameterName))
        }
        bla.NamedParameters[ ParameterName ] = ParameterValue
        } else { 
        fname := flag.Arg( i )
        if EndOfParameterName != -1 {
            fname = flag.Arg( i )[:EndOfParameterName] + flag.Arg( i )[EndOfParameterName+1:]
        } 
        if templateName == "" {
           templateName = fname
        } else if writeTo == "" {
           writeTo = fname
        } else {
           fmt.Fprintln(os.Stderr, "Superfluous argument to command-line [", fname, "]")
        }
        }
    }

    var bytes []byte
    var err   error

    if templateName == "-" {
      if bytes, err = ioutil.ReadAll( os.Stdin ); err != nil {
         fmt.Fprintln(os.Stderr, fmt.Sprintf( "Reading %s failed (%v)", templateName, err ) )
         os.Exit( 1 )
      }
    } else {
      if bytes, err = ioutil.ReadFile( templateName ); err != nil {
         fmt.Fprintln(os.Stderr, fmt.Sprintf( "Reading %s failed (%v)", templateName, err ) )
         os.Exit( 1 )
      }
    }
    content := string( bytes )
    if !strings.HasSuffix( content, "\n" ) {
            content += "\n"
    }

    var conn *sql.DB
    if conn, err = bla.DbConnect( dbUrl, bla.InitialSQLString ); err != nil {
       fmt.Fprintln(os.Stderr, fmt.Sprintf( "Connecting to database failed (%v)", err ) )
       os.Exit( 1 )
    }

    var reduced string
    if reduced, err = bla.Reduce( content, conn ); err != nil {
                // close database as soon as possible as connections can be scarce; we can't use defer
        conn.Close()
        fmt.Fprintln(os.Stderr, fmt.Sprintf( "Reducing failed (%v)", err ) )
        os.Exit(1)
    } else {
        conn.Close()
    }

    if writeTo == "-" {
       fmt.Print( reduced )
    } else {
       if err = ioutil.WriteFile( writeTo, []byte(reduced), 0770 ); err != nil {
           fmt.Fprintln(os.Stderr, fmt.Sprintf( "Writing output failed (%v)", err ) )
           os.Exit(1)
       }
    }
}
