package reducer


import "bytes"
import "errors"
import "regexp"
import "os"
import "fmt"
import "strings"
import "strconv"
import "database/sql"
import _ "github.com/lib/pq"
import _ "github.com/mattn/go-sqlite3"

type Reducer struct {
   Verbose           bool
   NamedParameters   map[string]string
   DriverName        string
   Connection        *sql.DB
   CommentSymbol     string
   InitialSQLString  string
}
func NewReducer() (rcvr *Reducer) {
   rcvr = &Reducer{}
   rcvr.NamedParameters = make(map[string]string)
   return rcvr
}

func (rcvr *Reducer) logInfo(msg string, extra string) {
        if rcvr.Verbose {
                fmt.Fprintln(os.Stderr, fmt.Sprintf("%v%v", msg, extra))
        }
}
func (rcvr *Reducer) DbConnect( url string, initialSQL string) (*sql.DB, error) {
   var err error 
   var db *sql.DB
   driver := "sqlite3"
   if strings.HasPrefix( url, "postgres:" ) {
       driver = "postgres"
   }
   rcvr.DriverName = driver
   if db, err = sql.Open( driver, url ); err != nil {
       return nil, err
   }
   if initialSQL != "" {
       if _, err = db.Exec( initialSQL ); err != nil {
           fmt.Printf( "Bad result sending initial SQL %v\n", err )
           return nil, err
       }
   }
   if err = db.Ping(); err != nil {
       fmt.Printf( "No ping possible %v\n", err )
       return nil, err
   }
   return db, nil
}
func Escape(sqlText string) (string) {
   if len( sqlText ) == 0 {
           return ""
   }
   ret := ""
   index := 0
   for index < len( sqlText ) {
           ch := sqlText[index]
           switch ch {
           case '\'':
                   ret += "\\"
           case '\\':
                   ret += "\\"
           case '&':
                   ret += "\\"
           }
           ret += string( ch )
           index += 1
   }
   return ret
}
func (rcvr *Reducer) EvaluateStatements( template string ) (string, error) {
    b, err := rcvr.FirstPass( template )
    source := b.String()
    if err != nil {
        return "", err
    }
    result := &bytes.Buffer{}
    source, result = rcvr.advanceToNextCommandOrEnd( source, result )
    thingstodo:
    for source != "" {
        rcvr.logInfo( "evaluating (first 10 chars): ", source[0:min(len(source),10)])
        switch {
         case strings.HasPrefix( source, "@sqlselect" ), strings.HasPrefix( source, "@sqlwith" ):
            if source, result, err = rcvr.handleEvalSqlStatement( source, result, nil ); err != nil {
                break thingstodo
            }
         case strings.HasPrefix( source, "@field<" ):
            err = errors.New( "A field at top level is not a valid construct." )
            break thingstodo
         case strings.HasPrefix( source, "@quotedField<" ):
            err = errors.New( "A quoted field at top level is not a valid construct." )
            break thingstodo
         default: 
            source, result = rcvr.advanceToNextCommandOrEnd( source, result )
        }
        rcvr.logInfo( "loop expnd", "")
    }
    return result.String(), err
}
func (rcvr *Reducer) EvaluateSqlStatement(source string) (string, error) {
    startOfSqlTarget := strings.Index( source, ";" ) + 1
    if startOfSqlTarget == 0 {
      return "", errors.New( "select doesn't end with ';'" )
    }
    var sqlCommand string
    if rcvr.DriverName == "oci8" { // oracle doesn't like the ';' at the end
      sqlCommand = source[ :startOfSqlTarget-1 ]
    } else {
      sqlCommand = source[ :startOfSqlTarget ]
    }
    result := &bytes.Buffer{}
    result.WriteString( rcvr.asComment(fmt.Sprintf("%v%v%v", " @sql: ", sqlCommand, '\n')) )

    rcvr.logInfo("sql for query: ", sqlCommand)
    rows, err := rcvr.Connection.Query( sqlCommand )
    if err != nil {
      rcvr.logInfo( "Query failed", "")
      return "", err
    }
    defer rows.Close()

    sqlResultTypes, _ := rows.ColumnTypes()
    Cols, _ := rows.Columns()
    rcvr.logInfo("Number of columns: ", fmt.Sprintf("%d", len( Cols )))
    for i := 0; i < len( Cols ); i++ {
      rcvr.logInfo(fmt.Sprintf("%v%v%v%v", sqlResultTypes[i].Name()+"\t", 
                          sqlResultTypes[i].ScanType(), ":", 
                          sqlResultTypes[i].DatabaseTypeName()), "")
    }
    noRows := 0
    sqlTarget := source[startOfSqlTarget:]
    for rows.Next() {
      noRows += 1
      rcvr.logInfo("row ", fmt.Sprintf( "%d", noRows))
      source := sqlTarget
      rowvaluepointers := make( []interface{}, len( Cols ) )
      rowvalues := make( []sql.NullString, len( Cols ) )
      for i, _ := range rowvalues {
         rowvaluepointers[ i ] = &rowvalues[ i ]
      }
      err = rows.Scan( rowvaluepointers... )
      if err != nil && err == sql.ErrNoRows { // no rows is actually an error! finished then...
          result.WriteString( rcvr.asComment( fmt.Sprintf("%v%v%v", "@end: ", 
                                                            sqlCommand, '\n')))
          return result.String(), nil           
      }
      if err != nil {
        return "", err
      }
      namedvalues := make(map[string]string)
      for c, _ := range rowvalues {
          if rowvalues[ c ].Valid {
               namedvalues[ Cols[ c ] ] = rowvalues[ c ].String
          } else {
               namedvalues[ Cols[ c ] ] = "" // this solves null-values not ending up in namedvalues array
          }
      }
      if len( Cols ) != len( rowvalues ) {
        rcvr.logInfo( "mismatch in number of cols and number of rowvalues", "" )
      }
      for source != "" {
        switch {
          case strings.HasPrefix( source, "@sqlselect" ), strings.HasPrefix( source, "@sqlwith" ):
            if source, result, err = rcvr.handleEvalSqlStatement( source, result, namedvalues ); err != nil {
               return result.String(), err
            }
          case strings.HasPrefix( source, "@field<"):
            if source, result, err = rcvr.handleField( source, result, namedvalues, true ); err != nil {
               return "", err
            }
          case strings.HasPrefix( source, "@quotedField<" ):
            if source, result, err = rcvr.handleQuotedField( source, result, namedvalues, true ); err != nil {
               return "", err
            }
          default:
            source, result = rcvr.advanceToNextCommandOrEnd( source, result )
        }
      }
      rcvr.logInfo("loop rows.Next()", "")
    }
    return result.String(), nil
}
func (rcvr *Reducer) FirstPass(template string) (*bytes.Buffer, error) {
    var err error
    source := template
    result := &bytes.Buffer{}
    rcvr.logInfo( "FirstPass start", "" )
    source, result = rcvr.advanceToNextCommandOrEnd( source, result )
    for source != ""  {
        rcvr.logInfo( "firstpass parsing (first 10 chars): ", source[0:min(len(source),10)])
        switch {
          case strings.HasPrefix( source, "@quote<" ):
               source, result = rcvr.handleQuote( source, result )
          case strings.HasPrefix( source, "@param<" ):
            if source, result, err = rcvr.handleParam( source, result ); err != nil {
                return result, err // was "", err
            }
          case strings.HasPrefix( source, "@comment"):
            if source, result, err = rcvr.handleComment( source, result ); err != nil {
               return result, err
            }
          case strings.HasPrefix( source, "@"): // some command, but we'll skip it
            _, _ = result.WriteString( string( source[ 0 ] ) )
            source = source[ 1: ]
            source, result = rcvr.advanceToNextCommandOrEnd( source, result )
          default:
            source, result = rcvr.advanceToNextCommandOrEnd( source, result )
        }
        //rcvr.logInfo( "firstpass result (first 20 chars): ", result.String()[0:min(len(result.String()),10)])
    }
    rcvr.logInfo( "FirstPass end", result.String() )
    return result, nil
}
func (rcvr *Reducer) GetTheQuote(source string) (int, int, string) {
   var startQuote int
   if startQuote = strings.Index( source, "@quote<" ); startQuote == -1 {
       return -1, -1, ""
   }
   source = source[ startQuote + len( "@quote<" ): ] // we isolated the beginning of the to-be-quoted-string
   endQuote := startQuote + len( "@quote<" )
   result := ""
   breakout:
   for source != "" {
     switch {
       case source[ 0 ] == '\\':
         if source[ 1 ] == '<' || source[ 1 ] == '>' {
           endQuote += 2
           result += string( source[ 1 ] ) // skip 0
           source = source[ 2: ]
         } else {
           endQuote += 1
           result += string( source[ 0 ] ) // dont skip
           source = source[ 1: ]
         }
       case source[ 0 ] == '>':
         break breakout
       default:
         endQuote += 1
         result += string( source[ 0 ] ) // dont skip
         source = source[ 1: ]
     }
   }
   if source != "" && source[ 0 ] == '>' {
     return startQuote, endQuote + 1, result
   } else {
     return -1, -1, ""
   }
}
func (rcvr *Reducer) GetEndOfParam(source string) (int) {
   if startParam := strings.Index( source, "@param<" ); startParam == -1 {
     return -1
   } else {
     startParam += len( "@param<" )
     if endParam := strings.Index( source[ startParam: ], ">" ); endParam == -1 {
       return -1
     } else {
       endParam += startParam
       return endParam
     }
   }
}
func (rcvr *Reducer) IndexSQLReduceCommand( source string) (int) {
   selpos := strings.Index( source, "@sqlselect" )
   witpos := strings.Index( source, "@sqlwith" )
   if selpos == -1 { // select not found, return with, possibly -1
     return witpos
   }
   if witpos == -1 { // no with, there is a select, return it
     return selpos
   }
   return min( selpos, witpos ) // return first command encountered
}
func (rcvr *Reducer) GetEndOfSqlStatement(source string) (endOfStatement int) {
   startOfStatement := rcvr.IndexSQLReduceCommand( source )
   if startOfStatement == -1 {
       return -1
   }
   startOfStatement += len( "@sql" ) // the SQL code starts after the prefix
   nestedStatement := startOfStatement
   for {
       relpos := strings.Index( source[ endOfStatement+1: ], "@sqlend" )
       if relpos != -1 {
           endOfStatement = endOfStatement + 1 + relpos 
       } else {
           endOfStatement = len( source )
       }
       if relpos = rcvr.IndexSQLReduceCommand( source[ nestedStatement+1:] ); relpos != -1 {
           nestedStatement = nestedStatement + 1 + relpos
       }
       if !(relpos != -1 && nestedStatement < endOfStatement) {
           break
       }
       rcvr.logInfo("loop getendofsqlclause", "")
   }
   return
}
func (rcvr *Reducer) hasAnyReduceCommand( source string ) bool {
    if found, err := regexp.MatchString( "^@field<|^@quotedField<|^@quote<|^@sqlselect|^@sqlwith|^@comment|^@param|^@comment", source ); err != nil {
       rcvr.logInfo( "Error matched using regexp", source )
       return false
    } else {
       return found
    }
}
func (rcvr *Reducer) GetNextCommandPosition(source string) int {
   var nextAt int
   if nextAt = strings.Index( source, "@" ); nextAt == -1 {
       return len( source )
   }
   found := false
   for { // there is a @ at nextAt
       found = rcvr.hasAnyReduceCommand( source[ nextAt: ] )
       if !found { // no luck, unknown @-command found, look furtheron
           rcvr.logInfo( "not a reduce command at:", strconv.Itoa( nextAt ) )
           if res := strings.Index( source[ nextAt + 1: ], "@" ); res != -1 {
              nextAt += res + 1
              rcvr.logInfo( "nextAt moves along to next @:", strconv.Itoa( nextAt ) )
           } else {
              nextAt = -1
              break
           }
       } else {
         break
       }
       rcvr.logInfo("loop getNextCommandPosition, nextAt: ", strconv.Itoa( nextAt ) )
   }
   if nextAt == -1 {
       return len( source )
   }
   return nextAt
}
func (rcvr *Reducer) GetFieldName(source string) (string) {
   if startOfName := strings.Index( source, "@field<" ); startOfName == -1 {
     return ""
   } else {
     startOfName += len( "@field<" )
     toreturn  := source[ startOfName: ]
     if endOfName := strings.Index( toreturn, ">" ); endOfName == -1 {
             return ""
     } else {
       return toreturn[ :endOfName ]
     }
   }
}
func (rcvr *Reducer) GetQuotedFieldName(source string) (string) {
   if startOfName := strings.Index( source, "@quotedField<" ); startOfName == -1 {
       return ""
   } else {
     startOfName += len( "@quotedField<" )
     toreturn := source[ startOfName: ]
     if endOfName := strings.Index( toreturn, ">" ); endOfName == -1 {
       return ""
     } else {
       return toreturn[ :endOfName ]
     }
   }
}

func (rcvr *Reducer) GetParamName(source string) (string) {
   if startParam := strings.Index( source, "@param<" ); startParam == -1 {
       return "";
   } else {
     startParam += len( "@param<" )
     if endParam := rcvr.GetEndOfParam( source ); endParam == -1 {
       return "";
     } else {
       return source[ startParam:endParam ]
     }
   }
}
func (rcvr *Reducer) GetSqlStatement(source string) (string) {
   if !strings.HasPrefix( source, "@sql" ) {
      rcvr.logInfo( "SQL Reduce command does not start with sql-prefix", "")
      return ""
   }
   startOfStatement := rcvr.IndexSQLReduceCommand( source )
   startOfStatement += len( "@sql" )
   if ending := rcvr.GetEndOfSqlStatement(source); ending == -1 || ending == len( source ) {
      return ""
   } else {
      return source[ startOfStatement:ending ]
   }
}
func Quote(colContents string) (string) {
   ret := "null"
   if len( colContents ) != 0 && colContents != "null" {
           ret = "'" + Escape(colContents) + "'"
   }
   return ret
}
func (rcvr *Reducer) RemoveFieldCommand(source string) (string, error) {
   if startOfName := strings.Index( source, "@field<" ); startOfName == -1 {
     return "", errors.New("@field< expected but not found")
   } else {
     startOfName +=  len( "@field<" )
     if endOfName := strings.Index( source[startOfName:], ">" ); endOfName == -1 {
       return "", fmt.Errorf("%v%v", "parameter doesn't end: ", source[startOfName:])
     } else {
       endOfName += startOfName
       return source[ endOfName+1: ], nil
     }
   }
}
func (rcvr *Reducer) RemoveQuotedFieldCommand(source string) (string, error) {
   if startOfName := strings.Index( source, "@quotedField<" ); startOfName == -1 {
     return "", errors.New("@quotedField< expected but not found")
   } else {
     startOfName +=  len( "@quotedField<" )
     if endOfName := strings.Index( source[startOfName:], ">" ); endOfName == -1 {
       return "", fmt.Errorf("%v%v", "parameter doesn't end: ", source[startOfName:])
     } else {
       endOfName += startOfName
       return source[ endOfName+1: ], nil
     }
   }
}
func (rcvr *Reducer) RemoveSqlStatement(source string) (string) {
   if endOfSqlStatement := rcvr.GetEndOfSqlStatement(source); endOfSqlStatement < 0 {
     return source
   } else {
      endOfSqlStatement += len( "@sqlend" )
      if endOfSqlStatement < len( source ) {
        return source[endOfSqlStatement:]
      } else {
        return ""
      }
   }
}
func (rcvr *Reducer) asComment(original string) (string) {
  if rcvr.CommentSymbol != "" && original != "" {
      result := ""
      source := original
      source = strings.Trim( source, "\t \n" )
      if !strings.HasPrefix(source,rcvr.CommentSymbol) {
         result = rcvr.CommentSymbol
      }
      for source != "" {
         if eol := strings.Index( source, "\n" ); eol == -1 {
           result += source
           source = ""
         } else {
           result += source[ :eol ] + "\n" + rcvr.CommentSymbol
           source = source[ eol + 1: ]
         }
      }
      return result
   }
   return ""
}
func (rcvr *Reducer) handleParam( source string, resultString *bytes.Buffer ) (string,*bytes.Buffer,error) {
   paramName := rcvr.GetParamName( source )
   if paramValue, ok := rcvr.NamedParameters[ paramName ]; !ok {
     return source[ len( "@param<" ) + len( paramName ) + 1: ], 
            resultString, 
            errors.New( fmt.Sprintf("Mismatch of used parameter with any given parameters %s", paramName) )
   } else { // removeParam should be there too
     _, _ = resultString.WriteString( paramValue )
     return source[ len( "@param<" ) + len( paramName ) + 1: ], resultString, nil
   }
}
func (rcvr *Reducer) handleComment( source string, resultString *bytes.Buffer ) (string,*bytes.Buffer,error) {
                         // now two cases: newline: dont copy newline from output, 
                         //                other chars: dont copy stuff until end of line, but do copy end of line
   source = source[ len( "@comment" ): ]
   if source[ 0 ] == '\n' {
       rcvr.logInfo( "comment with just a newline encountered, skipping newline", "" )
       return source[ 1: ], resultString, nil // skip newline
   } 
   var eol int
   if eol = strings.Index( source, "\n" ); eol == -1 {
     return "", resultString, errors.New( "No newline terminating the comment command, fatal")
   }
   rcvr.logInfo( "comment with text encountered skipping chars", strconv.Itoa( eol ) )
   return source[ eol: ], resultString, nil
}
func (rcvr *Reducer) handleEvalSqlStatement( source string, result *bytes.Buffer, row map[string]string ) (string,*bytes.Buffer,error) {
   var err error
   tmpString := ""
   evalled := rcvr.GetSqlStatement( source )
   rcvr.logInfo("select to reduce: ", evalled )
   if row != nil {
     if evalled, err = rcvr.ReduceNestedStatement( evalled, row ); err != nil {
        return source, result, err
     }
   }
   rcvr.logInfo("select to evaluate: ", evalled )
   if tmpString, err = rcvr.EvaluateSqlStatement( evalled ); err != nil {
      return source, result, err
   }
   rcvr.logInfo("reduced select: ", tmpString )
   result.WriteString( tmpString )
   return rcvr.RemoveSqlStatement( source ), result, err
}
func (rcvr *Reducer) handleReduceSqlStatement( source string, result *bytes.Buffer, row map[string]string ) (string,*bytes.Buffer,error) {
   var err error
   tmpString := ""
   if tmpString, err = rcvr.ReduceNestedStatement( rcvr.GetSqlStatement(source), row ); err != nil {
      return source, result, err
   }
   rcvr.logInfo("copied SQL: ", tmpString)
   result.WriteString( fmt.Sprintf("%v%v%v", "@sql", tmpString, "@sqlend") )
   return rcvr.RemoveSqlStatement(source), result, err
}
func (rcvr *Reducer) handleQuote( source string, result *bytes.Buffer ) (string,*bytes.Buffer) {
   start, end, tmpString := rcvr.GetTheQuote(source)
   rcvr.logInfo("quoted string: ", tmpString)
   _, _ = result.WriteString( tmpString )
   return source[ :start ] + source[ end: ], result
}
func (rcvr *Reducer) advanceToNextCommandOrEnd( source string, result *bytes.Buffer ) (string,*bytes.Buffer) {
   var endOfTopScopePart int
   endOfTopScopePart = rcvr.GetNextCommandPosition( source )
   tmpString := source[ :endOfTopScopePart ]
   rcvr.logInfo("copied text: ", tmpString)
   _, _ = result.WriteString( tmpString )
   return source[ endOfTopScopePart: ], result
}
func (rcvr *Reducer) handleField( source string, result *bytes.Buffer, rows map[string]string, errorout bool ) (string,*bytes.Buffer,error) {
    var err error
    fieldName := rcvr.GetFieldName( source )
    if source, err = rcvr.RemoveFieldCommand( source ); err != nil {
       return source, result, err
    }
    if tmpString, ok := rows[ fieldName ]; !ok {
        rcvr.logInfo("touched unreducable field, putting it back in: ", fieldName)
        if errorout {
           return source, result, fmt.Errorf( "Could not find column [%s]", fieldName )
        } else { // we're just reducing, not resolving, this column may exist later
           result.WriteString( "@field<" + fieldName + ">" )
           return source, result, nil
        }
    } else {
        rcvr.logInfo(fmt.Sprintf("%v%v%v", "reduced field ", fieldName, " to "), tmpString)
        result.WriteString( tmpString )
        return source, result, nil
    }
}
func (rcvr *Reducer) handleQuotedField( source string, result *bytes.Buffer, rows map[string]string, errorout bool ) (string,*bytes.Buffer,error) {
    var err error
    fieldName := rcvr.GetQuotedFieldName( source )
    if source, err = rcvr.RemoveQuotedFieldCommand( source ); err != nil {
       return source, result, err
    }
    if tmpString, ok := rows[ fieldName ]; !ok {
        rcvr.logInfo("touched unreducable quoted field, putting it back in: ", fieldName)
        if errorout {
           return source, result, fmt.Errorf( "Could not find column [%s]", fieldName )
        } else { // we're just reducing, not resolving, this column may be reducable later
           result.WriteString( "@quotedField<" + fieldName + ">" )
           return source, result, nil
        }
    } else {
        rcvr.logInfo(fmt.Sprintf("%v%v%v", "reduced quotedField ", fieldName, " to "), tmpString)
        result.WriteString( Quote(tmpString) )
        return source, result, nil
    }
}
func (rcvr *Reducer) Reduce( template string, conn *sql.DB) (string, error) {
     rcvr.Connection = conn 
     return rcvr.EvaluateStatements( template )
}

func (rcvr *Reducer) ReduceNestedStatement(source string, rows map[string]string) (string, error) {
    result := &bytes.Buffer{}
    var err error
    for source != "" {
      switch {
        case strings.HasPrefix( source, "@sqlselect" ), strings.HasPrefix( source, "@sqlwith" ) :
          if source, result, err = rcvr.handleReduceSqlStatement( source, result, rows ); err != nil {
             return result.String(), err
          }
        case strings.HasPrefix( source, "@field<" ) :
          if source, result, err = rcvr.handleField( source, result, rows, false ); err != nil {
             return result.String(), err
          }
        case strings.HasPrefix( source, "@quotedField<" ):
          if source, result, err = rcvr.handleQuotedField( source, result, rows, false ); err != nil {
             return result.String(), err
          }
        default :
          source, result = rcvr.advanceToNextCommandOrEnd( source, result )
      }
    }
    rcvr.logInfo( "result of nested clause:\n", "--%<---%<---\n")
    rcvr.logInfo( result.String(), "\n---->%---->%----")
    return result.String(), nil
}
func min(a int, b int) (int) {
  if a < b {
    return a
  }
  return b
}
