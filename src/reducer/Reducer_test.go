package reducer


import "testing"

import "strings"
import "fmt"
import "database/sql"
import "io/ioutil"

func readIt( name string, appendnl bool ) string {
   var bytes []byte
   var err   error
   if bytes, err = ioutil.ReadFile( name ); err != nil {
      return ""
   }
   content := string( bytes )
   if appendnl {
      if !strings.HasSuffix( content, "\n" ) {
	   content += "\n"
      }
   }
   return content
}
func TestOne(t *testing.T) {
   var conn *sql.DB
   var err  error
   bla   := NewReducer()

   if conn, err = bla.DbConnect( "../../test/t.db", "" ); err != nil {
      t.Error( "Connecting to database failed", err )
   }
   defer conn.Close()

   bla.CommentSymbol = ""
   if str := bla.RemoveSqlStatement( "@quote<bl di bl>aap" ); str != "@quote<bl di bl>aap" {
      fmt.Println( str )
      t.Error( "RemoveSqlStatement didnt work well" )
   }

   if Escape ("" ) != "" {
      t.Error( "Escape doesnt do empty string well")
   }
   if Escape (`\\bye` ) != `\\\\bye` {
      fmt.Println( Escape( `\\\\bye`) )
      t.Error( `Escape doesnt do \\bye string well`)
   }
   if Escape (`'` ) != `\'` {
      fmt.Println( Escape( `'`) )
      t.Error( `Escape doesnt do ' string well`)
   }
   if Escape (`\bye` ) != `\\bye` {
      fmt.Println( Escape( `\bye`) )
      t.Error( `Escape doesnt do \bye string well`)
   }
   if Quote( "aap" ) != "'aap'" {
      t.Error( "Quote doesnt work well")
      fmt.Println( Quote( "aap") )
   }

   _,_,str := bla.GetTheQuote( `@quote<bla\<ldld\>oror>` )
   if str != `bla<ldld>oror` {
      fmt.Println( "[" + str + "]" )
      t.Error( "GetTheQuote doesnt work well while escaping")
   }
   if _,end,_ := bla.GetTheQuote( "aap" ); end != -1 {
      t.Error( "EndOfQuote doesnt work well")
   }

   if bla.GetEndOfParam( "aap" ) != -1 {
      t.Error( "EndOfParam doesnt work well")
   }
   if bla.GetEndOfSqlStatement( "aap" ) != -1 {
      t.Error( "GetEndOfSqlStatement doesnt work well")
   }
   if bla.GetParamName( "bla" ) != "" {
      t.Error( "GetFieldName doesnt reject not a field")
   }
   if bla.GetParamName( "@param<bla" ) != "" {
      t.Error( "GetFieldName doesnt reject a field ref not ending")
   }
   if bla.GetFieldName( "bla" ) != "" {
      t.Error( "GetFieldName doesnt reject not a field")
   }
   if bla.GetFieldName( "@field<bla" ) != "" {
      t.Error( "GetFieldName doesnt reject a field ref not ending")
   }
   if bla.GetQuotedFieldName( "bla" ) != "" {
      t.Error( "GetQuotedFieldName doesnt reject not a field")
   }
   if bla.GetQuotedFieldName( "@quotedField<bla" ) != "" {
      t.Error( "GetQuotedFieldName doesnt reject a field ref not ending")
   }
   if _,_,str = bla.GetTheQuote( "bla" ); str != "" {
      t.Error( "GetTheQuote doesnt reject not a quote")
   }
   if _,_,str = bla.GetTheQuote( "@quote<bla" ); str != "" {
      t.Error( "GetTheQuote doesnt reject a field ref not ending")
   }
   if bla.GetSqlStatement( "aap @sqlselect aap, " ) != "" {
      t.Error( "GetSqlStatement accepts non-select")
   }
   if bla.GetSqlStatement( "@sqlselect aap, " ) != "" {
      t.Error( "GetSqlStatement accepts not ending statement")
   }
   if str, err := bla.RemoveFieldCommand( "@field<field>" ); err != nil || str != "" {
       t.Error("RemoveFieldCommand did it badly", str )
   }
   if _, err := bla.RemoveFieldCommand( "@bla<field>" ); err == nil {
       t.Error("RemoveFieldCommand accepted a non field", err )
   }
   if nxt := bla.GetNextCommandPosition( "äd@asads"); nxt != len( "äd@asads" ) {
       t.Error("GetNextCommandPosition didnt error out" )
   }
   var reduced, expected string
   content := readIt( "../../test/t1.in.txt", true )
   if reduced, err = bla.Reduce( content, conn ); err != nil {
       t.Error("Reducing failed t1", err )
   }
   expected = readIt( "../../test/t1.expected.txt", false )
   if reduced != expected {
       t.Error( "t1 failed" )
   }

   content = readIt( "../../test/t2.in.txt", true )
   if reduced, err = bla.Reduce( content, conn ); err == nil {
       t.Error("Reducing didnot fail t2", err )
   }

   content = readIt( "../../test/t3.in.txt", true )
   if reduced, err = bla.Reduce( content, conn ); err == nil {
       t.Error("Reducing didnot fail t3", err )
   }

   content = readIt( "../../test/t5.in.txt", true )
   if reduced, err = bla.Reduce( content, conn ); err == nil {
       t.Error("Reducing didnot fail t5", err )
   }

   bla.NamedParameters[ "oops" ] = "2"
   content = readIt( "../../test/t4.in.txt", true )
   if reduced, err = bla.Reduce( content, conn ); err != nil {
       t.Error("Reducing failed t4", err )
   }
   expected = readIt( "../../test/t4.expected.txt", false )
   if reduced != expected {
       t.Error( "t4 failed" )
       fmt.Println( reduced )
   }

   bla.CommentSymbol = "~"
   content = readIt( "../../test/t1.in.txt", true )
   if reduced, err = bla.Reduce( content, conn ); err != nil {
       t.Error("Reducing failed t7", err )
   }
   expected = readIt( "../../test/t7.expected.txt", true )
   if reduced != expected {
       t.Error( "t7 failed" )
       fmt.Println( reduced )
   }

   content = "@field<aap>"
   if reduced, err = bla.Reduce( content, conn ); err == nil {
       t.Error("Reducing didnot fail t8", err )
   }
   content = "@quotedField<aap>"
   if reduced, err = bla.Reduce( content, conn ); err == nil {
       t.Error("Reducing didnot fail t9", err )
   }

   content = "@sqlselect a, b from t1;@quotedField<aap>@sqlend"
   if reduced, err = bla.Reduce( content, conn ); err == nil {
       t.Error("Reducing didnot fail t10", err )
   }

   content = "@param<aap"
   if reduced, err = bla.Reduce( content, conn ); err == nil {
       t.Error("Reducing didnot fail t11", err )
   }

   content = "@param<aap>"
   if reduced, err = bla.Reduce( content, conn ); err == nil {
       t.Error("Reducing didnot fail t12", err )
   }

   bla.CommentSymbol = ""
   content = "@sqlselect 1 as a, null as b;@field<a>,@field<b>@sqlend"
   content += "\n"
   if reduced, err = bla.Reduce( content, conn ); err != nil {
       t.Error("Reducing failed t13", err )
   }
   expected = readIt( "../../test/t13.expected.txt", true )
   if reduced != expected {
       t.Error( "t13 failed" )
       fmt.Println( reduced )
   }

   content = readIt( "../../test/t14.in.txt", true )
   if reduced, err = bla.Reduce( content, conn ); err != nil {
       t.Error("Reducing failed t14", err )
   }
   expected = readIt( "../../test/t14.expected.txt", false )
   if reduced != expected {
       t.Error( "t14 failed" )
       fmt.Println( reduced )
   }

   conn.Close()
   if reduced, err = bla.Reduce( content, conn ); err == nil {
       t.Error("Reducing didnot fail closed t4", err )
   }
}
